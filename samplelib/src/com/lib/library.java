package com.lib;

public class library {

    private int bookid;
    private String bookname;
    private String bookauthor;
    private int bookrate;
    public library(int bookid,String bookname,String bookauthor,int bookrate)
    {
        this.bookid=bookid;
        this.bookname=bookname;
        this.bookauthor=bookauthor;
        this.bookrate=bookrate;

    }


    public void setBookid(int bookid)
    {
        this.bookid=bookid;
    }
    public int getBookid()
    {
        return this.bookid;
    }
    public void setBookname(String bookname)
    {
        this.bookname=bookname;
    }
    public String getsbookname()
    {
        return this.bookname;
    }
    public void setBookauthor(String bookauthor)
    {
        this.bookauthor=bookauthor;
    }
    public String getBookauthor()
    {
        return this.bookauthor;
    }
    public void setBookrate(int bookrate)
    {
        this.bookrate=bookrate;
    }
    public int getBookrate()
    {
        return this.bookrate;
    }



}

